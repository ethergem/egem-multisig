var txDefaultOrig =
{
  websites: {
    "wallet": "https://multisig.egem.io",
    "gnosis": "https://gnosis.pm",
    "ethGasStation": "https://api.egem.io/gas"
  },
  resources : {
    "termsOfUse": "https://multisig.egem.io/TermsofUseMultisig.pdf",
    "privacyPolicy": "https://gnosis.io/privacy-policy",
    "imprint": "https://multisig.egem.io/imprint.html"
  },
  gasLimit: 3141592,
  gasPrice: 18000000000,
  ethereumNode: "https://lb.rpc.egem.io:443",
  connectionChecker: {
    method : "OPTIONS",
    url : "https://www.google.com",
    checkInterval: 5000
  },
  accountsChecker: {
    checkInterval: 5000
  },
  transactionChecker: {
    checkInterval: 15000
  },
  wallet: "injected",
  defaultChainID: null,
  // Mainnet 0xC38c006a13F84881407bdFf5ecaB98D7d5F1F10D
  walletFactoryAddress: "0x6e95c8e8557abc08b46f3c347ba06f8dc012763f",
  tokens: [
    {
      'address': '0x4db342F129796EdC347C3E2AFF763E340ac85ebF',
      'name': 'Wrapped EGEM',
      'symbol': 'WEGEM',
      'decimals': 18
    }
  ]
};

if (isElectron) {
  txDefaultOrig.wallet = "remotenode";
}

var txDefault = {
  ethereumNodes : [
    {
      url : "https://lb.rpc.egem.io:443",
      name: "Remote Mainnet"
    },
    {
      url : "http://localhost:8895",
      name: "Local node"
    }
  ],
  walletFactoryAddresses: {
    'mainnet': {
      name: 'Mainnet',
      address: txDefaultOrig.walletFactoryAddress
    }
  }
};

var oldWalletFactoryAddresses = [
];

/**
* Update the default wallet factory address in local storage
*/
function checkWalletFactoryAddress() {
  var userConfig = JSON.parse(localStorage.getItem("userConfig"));

  if (userConfig && oldWalletFactoryAddresses.indexOf(userConfig.walletFactoryAddress.toLowerCase()) >= 0) {
    userConfig.walletFactoryAddress = txDefaultOrig.walletFactoryAddress;
    localStorage.setItem("userConfig", JSON.stringify(userConfig));
  }
}

/**
* Reload configuration
*/
function loadConfiguration () {
  var userConfig = JSON.parse(localStorage.getItem("userConfig"));
  Object.assign(txDefault, txDefaultOrig, userConfig);
}

checkWalletFactoryAddress();
loadConfiguration();
